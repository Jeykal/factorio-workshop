declare namespace Cypress {
    interface Chainable<Subject> {
        // MARK: - GLOBAL

        tickGame(): Chainable<any>
        longWait(): Chainable<any>

        // MARK: - GUI

        startGame(playerName: string, options: {}): Chainable<any>

        bankStoneAmount(): Chainable<any>

        bankIronOreAmount(): Chainable<any>

        bankIronPlateAmount(): Chainable<any>

        findFirstStone(): Chainable<any>

        findFirstIronDeposit(): Chainable<any>

        findFirstEmptyField(): Chainable<any>

        buildMine(): Chainable<any>

        mineResource(): Chainable<any>

        buildIronPlateFactory(): Chainable<any>

        // MARK: - API

        getCoordinatesOfFirstNonEmptyAvailableTileByType(mappedWorld, tileResourceType, occupancy): Chainable<any>

        resetGame(): Chainable<any>

        getWorldState(): Chainable<any>

        getAmountOfResourceFromBank(resourceType): Chainable<any>

        playerTurnViaApi(playerName, actionType, coordinates): Chainable<any>

        getCoordinatesOfFirstAvailableEmptyTile(mappedWorld): Chainable<any>

        getFirstAvailableResourceDepositTile(mappedWorld): Chainable<any>

        getAllMineableResources(mappedWorld): Chainable<any>
    }
}