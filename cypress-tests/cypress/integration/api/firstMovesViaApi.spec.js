import {resources, occupancy, playerActions, players} from "../../support/apiObjects";
import {getData} from "../../support/apiCommands";


context('Basic operations via api', () => {
    before(() => {
        cy.resetGame();
    });

    it('Mine first found stone deposit and check the bank stone resource increased by one', function () {
        cy.getWorldState().then((mappedWorld) => {
            cy.getCoordinatesOfFirstNonEmptyAvailableTileByType(mappedWorld, resources.stone, "None")
                .then((coordinates) => {
                    cy.playerTurnViaApi(players.berlin, playerActions.mineResource, coordinates);
                });
        }).tickGame();
        cy.getAmountOfResourceFromBank(resources.stone).should('be.at.least', 1);
    });

    it('Initialize stone production and check that the resources have increased', async function () {
        cy.getWorldState().then((mappedWorld) => {
            cy.getCoordinatesOfFirstNonEmptyAvailableTileByType(mappedWorld, resources.stone, "None")
                .then((coordinates) => {
                    let playerList = Object.values(players); // This cannot be done in a nicer way
                    playerList.forEach((item) => {
                        cy.playerTurnViaApi(item, playerActions.mineResource, coordinates);
                    });
                })
        }).tickGame();

        // Just trying async await
        const stoneAmount = await getData(resources.stone);
        cy.log(stoneAmount);
        cy.getAmountOfResourceFromBank(resources.stone).should('be.at.least', 9);
    });

    it('build some stone mines and try to build every type of factory', function () {
        cy.getWorldState().then((mappedWorld) => {
            cy.getCoordinatesOfFirstNonEmptyAvailableTileByType(mappedWorld, resources.stone, "None")
                .then((coordinates) => {
                    for (let i = 0; i < 15; i++) {
                        let playerList = Object.values(players); // This cannot be done in a nicer way
                        playerList.forEach((item) => {
                            cy.playerTurnViaApi(item, playerActions.mineResource, coordinates);
                        });
                        cy.tickGame();
                    }
                })
        });

        let occupancies = Object.values(occupancy);
        occupancies.forEach((item) => {
            cy.getWorldState().then((mappedWorld) => {
                cy.getCoordinatesOfFirstAvailableEmptyTile(mappedWorld).then((coordinates) => {
                    cy.playerTurnViaApi(players.professor, "Build" + item, coordinates).tickGame();
                    cy.log(coordinates);
                });
            });
        });
        cy.getAmountOfResourceFromBank(resources.stone).should('be.at.least', 70);
    });

    it('occupy every free resource with a mine', function () {
        cy.getWorldState().then((mappedWorld) => {
            let index;

            // ZMAPOVAT VSECHNY RESOURCY => mit array jenom resourcu a nad tim teprve iterovat


            /*
            cy.getFirstAvailableResourceDepositTile(mappedWorld).then((resourceTile) =>{
                index = mappedWorld.findIndex((item)=> {
                    return item === resourceTile
                });
                cy.playerTurnViaApi(players.professor, "BuildMine", resourceTile.coordinates);
                mappedWorld[index].building = "Mine";
                cy.log(mappedWorld);
                cy.log(resourceTile);
            });
            */
            cy.getAllMineableResources(mappedWorld).then((mappedResources) => {
                cy.log(mappedResources);

                let stoneDeposits = mappedResources.filter(tile => tile.resourceSite.Resource === "Stone");
                cy.log(stoneDeposits);

                // stoneDeposits.forEach((tile) => {
                //     cy.playerTurnViaApi(players.professor, "BuildMine", tile.coordinates)
                // });

                // mappedResources.forEach((tile) => {
                //     if (stoneDeposits.find(tile)) {
                //         mappedResources.pop(tile);
                //     }
                // });

                mappedResources.forEach((tile) => {
                    cy.playerTurnViaApi(players.professor, "BuildMine", tile.coordinates)
                })
            });
            // mappedWorld.forEach((item) => {
            //     index = mappedWorld.findIndex((item) => {
            //         return item === item;
            //     });
            // });
            //
            //
            // // TODO: INDEX HLEDANYHO Objektu v originalMap
            // cy.log(mappedWorld[5]);
            // let findMe = mappedWorld[5];
            // let lul = mappedWorld.findIndex((item) => {
            //     return item === findMe;
            // });
            // cy.log(lul);
            //
            //
            // let found = mappedWorld.find(function (element) {
            //     return element.building === occupancy && element.resourceSite.Resource === tileResourceType && element.resourceSite.Amount > 0;
            // });
        });
    });
});

// TODO: pokud chci stavet s vice hracema, tak musim predat kazdemu hraci unikatni coordinates, aby se nepokouseli stavet vsici na stejne misto
// TODO: neco jako METADATA, do kterych bych si propisoval zmeny. Me pri staveni nezajima, jestli jsemneco vytezil... jen ze uz nekde neco mam postaveny a nekde ne

// TODO: spocitat kolik potrebuu resourcu na win (da se udelat na zacatku, kdy si getnu celej svet) -> pak si prolezu resources, abych prisel na to, kolik DOLU budu potrebovat, abych co nejrychlejs a nejoptimalnejc vyhral