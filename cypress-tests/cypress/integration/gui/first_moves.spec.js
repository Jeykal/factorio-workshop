context('Mining by hand', () => {
    before(() => {
        cy.resetGame();
        cy.startGame('Berlin');
    });

    // find first stone deposit and create a stone mine
    it('Init Stone Production!', function () {
        for (let i = 0; i < 5; i++) {
            cy.findFirstStone()
                .click()
                .mineResource();
            cy.tickGame();
        }
        cy.findFirstStone()
            .click()
            .buildMine();
        cy.tickGame();

        cy.waitUntil(() => cy.bankStoneAmount().should('be', 15), {
            timeout: 10000,
            interval: 1000
        });
    });

    // find first iron deposit and create an iron mine
    it('Init Iron Ore Production!', function () {
        cy.findFirstIronDeposit()
            .click()
            .buildMine()
            .tickGame();

        cy.waitUntil(() => cy.bankIronOreAmount().should('be', 5), {
            timeout: 10000,
            interval: 1000
        });
    });

    // find first empty field and create an iron plate factory
    it('Init Iron Plate Production!', function () {
        cy.findFirstEmptyField()
            .click()
            .buildIronPlateFactory();
        cy.tickGame();

        cy.waitUntil(() => cy.bankIronPlateAmount().should('be', 3), {
            timeout: 10000,
            interval: 1000
        });
    });
});