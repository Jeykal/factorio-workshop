Cypress.Commands.add('resetGame', () => {
    cy.request('POST', 'http://localhost:8080/restart', {});
});

Cypress.Commands.add('getWorldState', () => {
    return cy.request({
        method: 'GET',
        url: 'http://localhost:8080/world',
        followRedirect: false,
        headers: {
            'accept': 'application/json'
        }
    }).then((response) => {
        let body = response.body;
        let parsedBody = JSON.parse(body);
        return mapTileGrid(parsedBody.tiles);
    });
});

Cypress.Commands.add('playerTurnViaApi', (playerName, actionType, coordinates) => {
    cy.request(
        'POST',
        'http://localhost:8080/turns',
        {PlayerName: playerName, action: actionType, coordinates: {X: coordinates.x, Y: coordinates.y}}
    )
});

Cypress.Commands.add('getAmountOfResourceFromBank', (resourceType) => {
    return cy.request(
        'GET',
        'http://localhost:8080/bank',
        {}
    ).then((response) => {
        let resource = response.body.find((element) => {
            return element.Resource === resourceType
        });
        return resource.Amount
    });
});

function mapTileGrid(tileArray) {
    let mappedTilesWithCoordinates = [];
    for (let i = 0; i < tileArray.length; i++) {
        for (let j = 0; j < tileArray[i].length; j++) {
            let coordinates = {
                x: '',
                y: ''
            };
            coordinates.x = j;
            coordinates.y = i;
            tileArray[i][j].coordinates = coordinates;
            mappedTilesWithCoordinates.push(tileArray[i][j]);
        }
    }
    return mappedTilesWithCoordinates;
}

Cypress.Commands.add('getCoordinatesOfFirstNonEmptyAvailableTileByType', (mappedWorld, tileResourceType, occupancy) => {
    return getCoordinatesOfFirstNonEmptyAvailableTileByType(mappedWorld, tileResourceType, occupancy);
});

function getCoordinatesOfFirstNonEmptyAvailableTileByType(mappedWorld, tileResourceType, occupancy) {
    let found = mappedWorld.find(function (element) {
        return element.building === occupancy && element.resourceSite.Resource === tileResourceType && element.resourceSite.Amount > 0;
    });
    return found.coordinates;
}

Cypress.Commands.add('getFirstAvailableResourceDepositTile', (mappedWorld) => {
    return getFirstAvailableResourceDepositTile(mappedWorld);
});

function getFirstAvailableResourceDepositTile(mappedWorld) {
    return mappedWorld.find(function (element) {
        return element.building === "None" && element.resourceSite.Amount > 0;
    });
}

Cypress.Commands.add('getAllMineableResources', (mappedWorld) => {
    return getAllMineableResources(mappedWorld)
});

function getAllMineableResources(mappedWorld) {
    return mappedWorld.filter(tile => tile.building === "None" && tile.resourceSite.Amount > 0);
}

Cypress.Commands.add('getCoordinatesOfFirstAvailableMineByResourceType', (mappedWorld, resourceType) => {
    return getCoordinatesOfFirstAvailableMineByResourceType(mappedWorld, resourceType);
});

function getCoordinatesOfFirstAvailableMineByResourceType(mappedWorld, resourceType) {
    let found = mappedWorld.find(function (element) {
        return element.building === "Mine" && element.resourceSite.Resource === resourceType;
    });
    return found.coordinates;
}

Cypress.Commands.add('getCoordsOfFirstAvailableBuildingByType', (mappedWorld, buildingType) => {
    return getCoordsOfFirstAvailableBuildingByType(mappedWorld, buildingType);
});

function getCoordsOfFirstAvailableBuildingByType(mappedWorld, buildingType) {
    let found = mappedWorld.find(function (element) {
        return element.building === buildingType;
    });
    return found.coordinates;
}

Cypress.Commands.add('getCoordinatesOfFirstAvailableEmptyTile', (mappedWorld) => {
    return getCoordinatesOfFirstAvailableEmptyTile(mappedWorld);
});

function getCoordinatesOfFirstAvailableEmptyTile(mappedWorld) {
    let found = mappedWorld.find(function (element) {
        return element.building === "None" && element.resourceSite.Resource === "None" && element.resourceSite.Amount === 0;
    });
    return found.coordinates;
}

// TODO: Make things more readable - can replace Cypress commands
export function getData(resourceType) {
    return new Promise((resolve) => {
        return cy.request(
            'GET',
            'http://localhost:8080/bank',
            {}
        ).then((response) => {
            let resource = response.body.find((element) => {
                return element.Resource === resourceType
            });
            resolve(resource.Amount);
        })
    })
}