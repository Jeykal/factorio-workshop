export const playerActions = {
    mineResource: "MineResource",
    buildMine: "BuildMine",
    destroyBuilding: "DestroyBuilding",
    build: "Build"
};

export const players = {
    nairobi: "Nairobi",
    tokyo: "Tokyo",
    berlin: "Berlin",
    denver: "Denver",
    helsinki: "Helsinki",
    rio: "Rio",
    moscow: "Moscow",
    oslo: "Oslo",
    professor: "Professor"
};

export const resources = {
    none: "None",
    stone: "Stone",
    coal: "Coal",
    ironOre: "IronOre",
    copperOre: "CopperOre",
    oil: "Oil"
};

export const occupancy = {
    ironPlateFactory: "IronPlateFactory",
    steelFactory: "SteelFactory",
    steelCasingFactory: "SteelCasingFactory",
    rocketShellFactory: "RocketShellFactory",
    copperPlateFactory: "CopperPlateFactory",
    wireFactory: "WireFactory",
    circuitFactory: "CircuitFactory",
    computerFactory: "ComputerFactory",
    petroleumFactory: "PetroleumFactory",
    basicFuelFactory: "BasicFuelFactory",
    solidFuelFactory: "SolidFuelFactory",
    rocketFuelFactory: "RocketFuelFactory"
};
