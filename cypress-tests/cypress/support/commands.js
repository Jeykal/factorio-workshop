// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import 'cypress-wait-until';

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', () => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('longWait', () => {
    cy.wait(3000);
});

Cypress.Commands.add('bankStoneAmount', () => {
    cy.scrollTo('top');
    return cy.get('#bank-Stone-amount').invoke('text');
});

Cypress.Commands.add('bankIronOreAmount', () => {
    return cy.get('#bank-IronOre-amount').invoke('text');
});

Cypress.Commands.add('bankIronPlateAmount', () => {
   return cy.get('#bank-IronPlate-amount').invoke('text');
});

Cypress.Commands.add('findFirstEmptyField', () => {
   return cy.get('td > div > img[alt="Icon of None"]:first');
});

Cypress.Commands.add('findFirstStone', () => {
    return cy.get('td > div > img[alt="Icon of Stone"]:first');
});

Cypress.Commands.add('findFirstIronDeposit', () => {
    return cy.get('td > div > img[alt="Icon of IronOre"]:first');
});

Cypress.Commands.add('mineResource', () => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', () => {
    cy.get('#action-BuildMine').click();
});

Cypress.Commands.add('buildIronPlateFactory', () => {
    cy.get('#action-BuildIronPlateFactory').click();
});

