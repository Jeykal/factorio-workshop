# Simplified Factorio App

### Architecture
The game is using standard server-client architecture and has 2 parts:
- Backend Server
- Frontend Application

##### Backend Server (app/server)
Stores and provides all logic of the game. Uses simple HTTP API and holds all information within memory (= be sure to run it with enough memory and shutting down will result in losing current game state). It works in "ticks" that occur once per second. In this tick, from queue of each player is polled only single action and is evaluated (if queue is empty, nothing happens).

The HTTP API provides these endpoints:
- GET /bank = Provides information about current bank (resources players have)
- GET /world = Provides information about current state of world (map / tiles)
- POST /turns = Adds a new player action to his/her queue (only one turn is evaluated per tick)
- POST /restart = Starts a new game

The server runs on port 8080 by default but this can be changed by passing one argument to binary. You can also pass `version` as first argument to find out version of server. E.g.
- `./server_linux_amd64 9090`
- `./server_linux_amd64 version`

##### Web Application (app/web)
Provides UI via browser for interaction with the server. It more or less passes all information to server without any additional logic.

The web application is static web content that needs to be hosted on any webserver (e.g. npm http-server, npm local-web-server, httpd, apache tomcat...). There is not specific requirement = it just needs to serve static content. E.g:
- `npm install local-web-server -g`
- `cd app/web`
- `ws`

The web application connects to server's default port 8080 but if other server port is used, it can be reconfigured in `config.js` file.

### Changelog

##### 2.0.0
- Added coal resource => all factories now require 1 coal (not only 3 lower resources) to produce something
- Changed UI for tile actions = dialog in center to avoid glitches and maintain consistency
- Fixed API that allowed to build a mine on a an empty tile
- Fixed server crash in case of invalid turn coordinates
- Fixed missing icons for steel casing
- Few UI tweaks

##### 1.0.0
- Initial version of application
